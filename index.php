<?php

require 'vendor/autoload.php';

define('APPLICATION_ROOT_DIR', __DIR__ . '/');

\MVF\ApiExchangeRateConverter\Gateway::bootstrap(new \Silex\Application());

