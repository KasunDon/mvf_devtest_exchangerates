NAME=mvf-exchange-rates-api

all: build

build:
	tar --exclude-from=.dockerignore -czf - . | docker build -t ${NAME} -f docker/Dockerfile -

start: stop
	docker run -d -p 80:80 --name ${NAME} \
	 ${NAME}

stop:
	docker rm -vf ${NAME} ||:

tests:
	docker exec -it ${NAME} vendor/bin/phpspec r -c tests/phpspec/phpspec.yml ||:
	docker exec -it ${NAME} vendor/bin/phpunit --config tests/phpunit/phpunit.xml ||:
	docker exec -it ${NAME} vendor/bin/phpcs -p --colors --standard=PSR2 src/ ||:

.PHONY: all tests build start stop