<?php

namespace ApiExchangeRateConverterTest\Helper;

use Swift_MemorySpool;

class MemorySpool extends Swift_MemorySpool
{
    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
