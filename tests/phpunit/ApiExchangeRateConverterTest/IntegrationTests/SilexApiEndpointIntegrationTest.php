<?php

namespace ApiExchangeRateConverterTest\IntegrationTests;

use ApiExchangeRateConverterTest\Helper\MemorySpool;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use MVF\ApiExchangeRateConverter\Gateway;
use Silex\Application;
use Silex\WebTestCase;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class SilexApiEndpointIntegrationTest extends WebTestCase
{
    /** @var MemorySpool */
    private $memorySpool;


    /** @var Gateway */
    private $gateway;

    /**
     * Creates the application.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        return $this->createApplicationWithMockingMailerAndFetcher();
    }

    /**
     * @param bool $mock [false]
     * @return Application
     */
    private function createApplicationWithMockingMailerAndFetcher($mock = false)
    {
        $this->gateway = new Gateway(new Application());
        $this->gateway->initializeServices();

        if ($mock) {
            $this->assignMockedServices();
        }

        $this->gateway->initializeRoutes();
        $this->gateway->setupErrorHandler();

        $app = $this->gateway->getApplication();
        $app['debug'] = true;

        return $app;
    }

    private function assignMockedServices()
    {
        $container = $this->gateway->getServiceContainer();

        $container->reset();

        // Injecting wrapper for Swift_MemorySpool class
        $this->memorySpool = new MemorySpool();
        $container->set('swift_mailer_memory_spool', $this->memorySpool);

        $mockedExchangeRateFetcher = $this
            ->getMockBuilder('MVF\ApiExchangeRateConverter\Domain\ExchangeRateFetcherInterface')
            ->getMock();

        $mockedExchangeRateFetcher->method('fetch')
            ->willReturn(array());

        $container->set('api_exchange_rate_converter_rest_rate_fetcher', $mockedExchangeRateFetcher);
    }

    public function test_default_endpoint_to_return_test_as_response()
    {
        $client = $this->createClient();
        $client->request('GET', '/test', array());
        $response = $client->getResponse();

        $expected = '{"test":"index"}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_update_exchange_rates_endpoint_should_return_ok_status_when_success()
    {
        $client = $this->createClient();
        $client->request('GET', '/test/update_exchange_rates', array());
        $response = $client->getResponse();

        $expected = '{"status":"OK"}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_update_exchange_rates_endpoint_should_return_failure_status_when_things_gone_wrong()
    {
        $this->app = $this->createApplicationWithMockingMailerAndFetcher(true);

        $client = $this->createClient();
        $client->request('GET', '/test/update_exchange_rates', array());
        $response = $client->getResponse();

        $expected = '{"status":"FAILED"}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_update_exchange_rates_endpoint_should_return_failure_email_when_things_gone_wrong()
    {
        $this->app = $this->createApplicationWithMockingMailerAndFetcher(true);

        $client = $this->createClient();
        $client->request('GET', '/test/update_exchange_rates', array());

        $subject = 'CRITICAL ERROR: Exchange rates update unsuccessful';
        $body = 'Exchange rates update unsuccessful from openexchangerates.org.';
        $sender = array('app-error@mvfglobal.com');
        $recipient = array('admin@mvfglobal.com');

        $sentMessages = $this->memorySpool->getMessages();
        $this->assertEquals($subject, $sentMessages[0]->getSubject());
        $this->assertEquals($body, $sentMessages[0]->getBody());
        $this->assertArrayHasKey($sender[0], $sentMessages[0]->getFrom());
        $this->assertArrayHasKey($recipient[0], $sentMessages[0]->getTo());
    }

    public function test_that_gbp_as_default_desired_currency_when_to_currency_parameter_is_missing()
    {
        $client = $this->createClient();
        $client->request('GET', '/test/convert_currency/100.00/GBP', array());
        $response = $client->getResponse();

        $expected = '{"from_currency":"GBP","to_currency":"GBP","exchange_rate":1,"original_amount":100,"converted_amount":100}';
        $this->assertEquals($expected, $response->getContent());
    }

    public function test_that_unknown_currency_wont_get_converted_when_passing_either_from_or_to_currency()
    {
        $client = $this->createClient();
        $client->request('GET', '/test/convert_currency/100/GBP/ABC', array());
        $response = $client->getResponse();

        $expected = '{"from_currency":"GBP","to_currency":"ABC","exchange_rate":0,"original_amount":100,"converted_amount":0}';
        $this->assertEquals($expected, $response->getContent());

        $client->request('GET', '/test/convert_currency/104/ABC/GBP', array());
        $response = $client->getResponse();

        $expected = '{"from_currency":"ABC","to_currency":"GBP","exchange_rate":0,"original_amount":104,"converted_amount":0}';
        $this->assertEquals($expected, $response->getContent());
    }

    public function test_that_non_string_currency_input_or_currency_length_not_equals_to_3_will_result_in_500_error_code(
    )
    {

        $client = $this->createClient();
        $client->request('GET', '/test/convert_currency/100/GBPP', array());
        $response = $client->getResponse();

        $expected = '{"error":"Invalid currency or currency not supported."}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(500, $response->getStatusCode());

        $client->request('GET', '/test/convert_currency/100/GBPP/ABC', array());
        $response = $client->getResponse();

        $expected = '{"error":"Invalid currency or currency not supported."}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(500, $response->getStatusCode());

        $client->request('GET', '/test/convert_currency/100/GBP/USDD', array());
        $response = $client->getResponse();

        $expected = '{"error":"Invalid currency or currency not supported."}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(500, $response->getStatusCode());
    }

    public function test_that_non_numeric_amount_input_will_result_in_500_error_code()
    {
        $client = $this->createClient();
        $client->request('GET', '/test/convert_currency/1ab2/GBP', array());
        $response = $client->getResponse();

        $expected = '{"error":"Invalid amount or given type not supported."}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(500, $response->getStatusCode());

        $client->request('GET', '/test/convert_currency/ /GBP', array());
        $response = $client->getResponse();

        $expected = '{"error":"Invalid amount or given type not supported."}';
        $this->assertEquals($expected, $response->getContent());
        $this->assertEquals(500, $response->getStatusCode());
    }

    public function test_that_it_should_convert_currency()
    {
        $client = $this->createClient();
        $client->request('GET', '/test/convert_currency/100/GBP/USD', array());
        $response = $client->getResponse();

        $result = json_decode($response->getContent());

        $exchangeRateRepository = $this->gateway
            ->getServiceContainer()
            ->get('api_exchange_rate_converter_exchange_rate_repository');

        $expectedFromCurrency = 'GBP';
        $expectedToCurrency = 'USD';

        $gbpRate = $exchangeRateRepository->findByCurrency(new Currency($expectedFromCurrency));
        $usdRate = $exchangeRateRepository->findByCurrency(new Currency($expectedToCurrency));

        $expectedRate = ($usdRate->getRate() / $gbpRate->getRate());
        $expectedOriginalAmount = 100;
        $expectedConvertedAmount = $expectedOriginalAmount * $expectedRate;

        $this->assertEquals($expectedFromCurrency, $result->from_currency);
        $this->assertEquals($expectedToCurrency, $result->to_currency);
        $this->assertEquals($expectedOriginalAmount, $result->original_amount);
        $this->assertEquals($expectedRate, $result->exchange_rate);
        $this->assertEquals($expectedConvertedAmount, $result->converted_amount);
    }

    protected function tearDown()
    {
        $this->gateway->getServiceContainer()->reset();
    }
}
