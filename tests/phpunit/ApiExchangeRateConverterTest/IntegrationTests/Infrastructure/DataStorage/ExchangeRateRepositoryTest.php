<?php

namespace ApiExchangeRateConverterTest\IntegrationTests\Infrastructure\DataStorage;

use Doctrine\ORM\EntityManager;
use MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use MVF\ApiExchangeRateConverter\Infrastructure\DI\AppServices;

class ExchangeRateRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var EntityManager */
    private $entityManager;

    /** @var ExchangeRateRepository */
    private $exchangeRateRepository;

    public function test_that_find_exchange_rate_by_currency()
    {
        $expectedCurrency = 'USD';
        $expectedRate = 1;

        $exchangeRate = $this->exchangeRateRepository->findByCurrency(new Currency($expectedCurrency));
        $this->assertEquals($expectedCurrency, $exchangeRate->getCurrency());
        $this->assertEquals($expectedRate, $exchangeRate->getRate());


        $value = $this->exchangeRateRepository->findByCurrency(new Currency('ABC'));
        $this->assertEquals(false, $value);
    }

    public function test_that_it_returns_pairs_of_currency_and_rate()
    {
        $expectedCount = 10;
        $expectedRateForUsd = 1;

        $results = $this->exchangeRateRepository->fetchAllCurrencyAndRate();

        $this->assertCount($expectedCount, $results);
        $this->assertArrayHasKey('ZAR', $results);
        $this->assertEquals($expectedRateForUsd, $results['USD']);
    }

    public function test_that_save_updates_to_persisting_entity()
    {
        $expectedCount = 10;
        $expectedCurrency = 'USD';
        $expectedRate = 2;

        $exchangeRate = $this->exchangeRateRepository->findByCurrency(new Currency($expectedCurrency));

        $exchangeRate->setRate($expectedRate);

        $this->exchangeRateRepository->save($exchangeRate);

        $totalRecords = $this->exchangeRateRepository->fetchAllCurrencyAndRate();

        $this->assertEquals($expectedCount, count($totalRecords));

        $exchangeRate = $this->exchangeRateRepository->findByCurrency(new Currency($expectedCurrency));
        $this->assertEquals($expectedCurrency, $exchangeRate->getCurrency());
        $this->assertEquals($expectedRate, $exchangeRate->getRate());

        $exchangeRate->setRate(1);
        $this->exchangeRateRepository->save($exchangeRate);
    }

    public function test_that_it_save_non_persisting_exchange_rate_entity()
    {
        $expectedCount = 11;

        $exchangeRate = new ExchangeRate();
        $exchangeRate->setRate(10);
        $exchangeRate->setUpdated(new \DateTime());
        $exchangeRate->setCurrency('ABC');

        $this->exchangeRateRepository->save($exchangeRate);

        $totalRecords = $this->exchangeRateRepository->fetchAllCurrencyAndRate();
        $this->assertEquals($expectedCount, count($totalRecords));

        $exchangeRate = $this->exchangeRateRepository->findByCurrency(new Currency('ABC'));

        $this->entityManager->remove($exchangeRate);
        $this->entityManager->flush();

        $totalRecords = $this->exchangeRateRepository->fetchAllCurrencyAndRate();
        $this->assertEquals(10, count($totalRecords));
    }

    protected function setUp()
    {
        $appServices = AppServices::initialize();
        $appServices->loadFromDefinitionFilePath();

        $container = $appServices->getContainer();

        $this->entityManager = $container->get('api_exchange_rate_converter_entity_manager');
        $this->exchangeRateRepository = $container->get('api_exchange_rate_converter_exchange_rate_repository');
    }
}
