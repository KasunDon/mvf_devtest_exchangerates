<?php

namespace ApiExchangeRateConverterTest\IntegrationTests\Infrastructure\DataStorage\Doctrine;

use Doctrine\ORM\EntityManager;
use MVF\ApiExchangeRateConverter\Infrastructure\DI\AppServices;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class EntityManagerIntegrationTest extends \PHPUnit_Framework_TestCase
{
    /** @var AppServices */
    private $appServices;

    /** @var ContainerBuilder */
    private $container;

    /** @var EntityManager */
    private $entityManager;

    public function test_exchange_rates_entity_to_fetch_a_record()
    {
        $exchangeRatesRepository = $this->entityManager
            ->getRepository('\MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate');

        $results = $exchangeRatesRepository->findOneBy(array('currency' => 'USD'));

        $this->assertEquals('USD', $results->getCurrency());
    }

    public function test_exchange_rates_entity_to_update_a_record()
    {
        $exchangeRatesRepository = $this->entityManager
            ->getRepository('\MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate');

        $results = $exchangeRatesRepository->findOneBy(array('currency' => 'USD'));

        $results->setRate(100);

        $this->entityManager->flush();

        $this->assertEquals('100', $results->getRate());

        $results = $exchangeRatesRepository->findOneBy(array('currency' => 'USD'));

        $results->setRate(1);

        $this->entityManager->flush();
    }

    protected function setUp()
    {
        $this->appServices = AppServices::initialize();
        $this->appServices->loadFromDefinitionFilePath();

        $this->container = $this->appServices->getContainer();

        $this->entityManager = $this->container->get('api_exchange_rate_converter_entity_manager');
    }
}
