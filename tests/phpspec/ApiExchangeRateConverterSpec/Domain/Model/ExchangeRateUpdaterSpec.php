<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\Model;

use MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateFetcherInterface;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use PhpSpec\ObjectBehavior;

class ExchangeRateUpdaterSpec extends ObjectBehavior
{
    function let(ExchangeRateFetcherInterface $exchangeRateFetcher, ExchangeRateRepository $exchangeRateRepository)
    {
        $this->beConstructedWith($exchangeRateFetcher, $exchangeRateRepository);
    }

    function it_should_update_exchange_rates_within_storage(
        ExchangeRateFetcherInterface $exchangeRateFetcher,
        ExchangeRateRepository $exchangeRateRepository,
        ExchangeRate $exchangeRate
    ) {
        $rawExchangeRates = array(
            'AED' => 3.672779,
            'AFN' => 67.3485,
            'ALL' => 122.740964,
            'CHF' => 482.494884,
            'JPY' => 1.773448,
            'GBP' => 165.9125,
            'ARS' => 15.352333,
            'USD' => 1
        );

        $avaliableExchangeRates = array(
            'USD' => 1,
            'GBP' => 0.7023,
            'JPY' => 100
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()
            ->willReturn($avaliableExchangeRates);


        $exchangeRateFetcher->fetch()
            ->shouldBeCalled()
            ->willReturn($rawExchangeRates);

        $exchangeRateRepository->findByCurrency(new Currency('JPY'))
            ->shouldBeCalled()
            ->willReturn($exchangeRate);
        $exchangeRateRepository->findByCurrency(new Currency('GBP'))
            ->shouldBeCalled()
            ->willReturn($exchangeRate);
        $exchangeRateRepository->findByCurrency(new Currency('USD'))
            ->shouldBeCalled()
            ->willReturn($exchangeRate);

        $exchangeRateRepository->save($exchangeRate)->shouldBeCalled();

        $this->update();
    }

    function it_should_throw_an_exception_if_there_are_no_updates(
        ExchangeRateFetcherInterface $exchangeRateFetcher,
        ExchangeRateRepository $exchangeRateRepository
    ) {
        $rawExchangeRates = array(
            'AED' => 3.672779,
            'AFN' => 67.3485,
            'ALL' => 122.740964,
            'CHF' => 482.494884,
            'ARS' => 15.352333,
        );

        $avaliableExchangeRates = array(
            'USD' => 1,
            'GBP' => 0.7023,
            'JPY' => 100
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()
            ->willReturn($avaliableExchangeRates);


        $exchangeRateFetcher->fetch()
            ->shouldBeCalled()
            ->willReturn($rawExchangeRates);


        $this->shouldThrow('\Exception')->duringUpdate();
    }

    function it_should_only_update_when_rate_is_greater_than_zero(
        ExchangeRateFetcherInterface $exchangeRateFetcher,
        ExchangeRateRepository $exchangeRateRepository,
        ExchangeRate $exchangeRate
    ) {
        $rawExchangeRates = array(
            'AED' => 3.672779,
            'AFN' => 67.3485,
            'ALL' => 122.740964,
            'CHF' => 482.494884,
            'JPY' => 0,
            'GBP' => 0,
            'ARS' => 15.352333,
            'USD' => 0
        );

        $avaliableExchangeRates = array(
            'USD' => 1,
            'GBP' => 0.7023,
            'JPY' => 100
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()
            ->willReturn($avaliableExchangeRates);


        $exchangeRateFetcher->fetch()
            ->shouldBeCalled()
            ->willReturn($rawExchangeRates);

        $exchangeRateRepository->findByCurrency(new Currency('JPY'))
            ->shouldNotBeCalled();

        $exchangeRateRepository->findByCurrency(new Currency('GBP'))
            ->shouldNotBeCalled();

        $exchangeRateRepository->findByCurrency(new Currency('USD'))
            ->shouldNotBeCalled();

        $exchangeRateRepository->save($exchangeRate)->shouldNotBeCalled();

        $this->update();
    }
}
