<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\Model;

use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;
use MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Amount;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use PhpSpec\ObjectBehavior;

class ExchangeRateConverterSpec extends ObjectBehavior
{
    function let(ExchangeRateRepository $exchangeRateRepository, ConfiguratorInterface $configurator)
    {
        $this->beConstructedWith($exchangeRateRepository, $configurator);
    }

    function it_should_convert_amount_from_given_currency_to_desired_currency(
        ExchangeRateRepository $exchangeRateRepository,
        ConfiguratorInterface $configurator,
        ExchangeRate $usdRate,
        ExchangeRate $gbpRate
    ) {
        $amount = new Amount(100);
        $originalCurrency = new Currency('USD');
        $desiredCurrency = new Currency('GBP');
        $excludedCurrencies = array('ABC');
        $allCurrencies = array(
            'ABC' => 21.0,
            'USD' => 1,
            'GBP' => 0.776165
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()->willReturn($allCurrencies);

        $configurator->get('parameters.excluded-currencies')
            ->shouldBeCalled()
            ->willReturn($configurator);

        $configurator->toArray()
            ->shouldBeCalled()
            ->willReturn($excludedCurrencies);

        $usdRate->getRate()->shouldBeCalled()->willReturn(1);
        $gbpRate->getRate()->shouldBeCalled()->willReturn(0.776165);

        $exchangeRateRepository->findByCurrency($originalCurrency)->shouldBeCalled()->willReturn($usdRate);
        $exchangeRateRepository->findByCurrency($desiredCurrency)->shouldBeCalled()->willReturn($gbpRate);

        $expectedOutput = array(
            Parameters::FROM_CURRENCY => $originalCurrency->getValue(),
            Parameters::TO_CURRENCY => $desiredCurrency->getValue(),
            Parameters::EXCHANGE_RATE => 0.776165,
            Parameters::ORIGINAL_AMOUNT => $amount->getValue(),
            Parameters::CONVERTED_AMOUNT => 77.6165

        );

        $this->convert($amount, $originalCurrency, $desiredCurrency)->shouldReturn($expectedOutput);
    }

    function it_should_return_zero_as_calculated_amount_when_requesting_known_currency(
        ExchangeRateRepository $exchangeRateRepository,
        ConfiguratorInterface $configurator
    ) {
        $amount = new Amount(100);
        $originalCurrency = new Currency('ABC');
        $desiredCurrency = new Currency('GBP');
        $excludedCurrencies = array('USD');
        $allCurrencies = array(
            'ABC' => 1.0,
            'USD' => 23.55,
            'GBP' => 2.22
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()->willReturn($allCurrencies);

        $configurator->get('parameters.excluded-currencies')
            ->shouldBeCalled()
            ->willReturn($configurator);

        $configurator->toArray()
            ->shouldBeCalled()
            ->willReturn($excludedCurrencies);


        $exchangeRateRepository->findByCurrency($originalCurrency)->shouldBeCalled()->willReturn(null);

        $expectedOutput = array(
            Parameters::FROM_CURRENCY => $originalCurrency->getValue(),
            Parameters::TO_CURRENCY => $desiredCurrency->getValue(),
            Parameters::EXCHANGE_RATE => 0,
            Parameters::ORIGINAL_AMOUNT => $amount->getValue(),
            Parameters::CONVERTED_AMOUNT => 0.0

        );

        $this->convert($amount, $originalCurrency, $desiredCurrency)->shouldReturn($expectedOutput);
    }

    function it_should_handle_hidden_currency_and_return_zero_as_converted_amount(
        ExchangeRateRepository $exchangeRateRepository,
        ConfiguratorInterface $configurator
    ) {
        $amount = new Amount(100);
        $originalCurrency = new Currency('ABC');
        $desiredCurrency = new Currency('USD');
        $excludedCurrencies = array('ABC');
        $allCurrencies = array(
            'ABC' => 1.0,
            'USD' => 23.55,
            'GBP' => 2.22
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()->willReturn($allCurrencies);

        $configurator->get('parameters.excluded-currencies')
            ->shouldBeCalled()
            ->willReturn($configurator);

        $configurator->toArray()
            ->shouldBeCalled()
            ->willReturn($excludedCurrencies);

        $expectedOutput = array(
            Parameters::FROM_CURRENCY => $originalCurrency->getValue(),
            Parameters::TO_CURRENCY => $desiredCurrency->getValue(),
            Parameters::EXCHANGE_RATE => 0,
            Parameters::ORIGINAL_AMOUNT => $amount->getValue(),
            Parameters::CONVERTED_AMOUNT => 0

        );

        $this->convert($amount, $originalCurrency, $desiredCurrency)->shouldReturn($expectedOutput);
    }

    function it_should_use_gbp_as_default_currency_when_desired_currency_not_given(
        ExchangeRateRepository $exchangeRateRepository,
        ConfiguratorInterface $configurator,
        ExchangeRate $usdRate,
        ExchangeRate $gbpRate
    ) {
        $amount = new Amount(100);
        $originalCurrency = new Currency('USD');
        $desiredCurrency = new Currency('GBP');

        $excludedCurrencies = array('ABC');
        $allCurrencies = array(
            'ABC' => 13.0,
            'USD' => 1,
            'GBP' => 0.776165
        );

        $exchangeRateRepository->fetchAllCurrencyAndRate()
            ->shouldBeCalled()->willReturn($allCurrencies);

        $configurator->get('parameters.excluded-currencies')
            ->shouldBeCalled()
            ->willReturn($configurator);

        $configurator->toArray()
            ->shouldBeCalled()
            ->willReturn($excludedCurrencies);

        $usdRate->getRate()->shouldBeCalled()->willReturn(1);
        $gbpRate->getRate()->shouldBeCalled()->willReturn(0.776165);

        $exchangeRateRepository->findByCurrency($originalCurrency)->shouldBeCalled()->willReturn($usdRate);
        $exchangeRateRepository->findByCurrency($desiredCurrency)->shouldBeCalled()->willReturn($gbpRate);

        $expectedOutput = array(
            Parameters::FROM_CURRENCY => $originalCurrency->getValue(),
            Parameters::TO_CURRENCY => $desiredCurrency->getValue(),
            Parameters::EXCHANGE_RATE => 0.776165,
            Parameters::ORIGINAL_AMOUNT => $amount->getValue(),
            Parameters::CONVERTED_AMOUNT => 77.6165

        );

        $this->convert($amount, $originalCurrency)->shouldReturn($expectedOutput);
    }
}
