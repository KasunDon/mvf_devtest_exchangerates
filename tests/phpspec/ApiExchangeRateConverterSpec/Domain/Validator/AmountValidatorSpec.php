<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\Validator;

use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use PhpSpec\ObjectBehavior;

class AmountValidatorSpec extends ObjectBehavior
{
    function it_should_implement_query_parameter_validator_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\QueryParameterValidator');
    }

    function it_should_allow_valid_amount(
        Parameters $parameters
    ) {
        $parameters->get(Parameters::AMOUNT)->shouldBeCalled()->willReturn(100);
        $this->validate($parameters);

        $parameters->get(Parameters::AMOUNT)->shouldBeCalled()->willReturn(100.05);
        $this->validate($parameters);
    }

    function it_should_throw_an_exception_when_amount_is_missing(
        Parameters $parameters
    ) {
        $parameters->get(Parameters::AMOUNT)->shouldBeCalled()->willReturn(null);
        $this->shouldThrow('\Exception')->duringValidate($parameters);
    }

    function it_should_throw_an_exception_when_non_numeric_values_passed_in(
        Parameters $parameters
    ) {
        $parameters->get(Parameters::AMOUNT)->shouldBeCalled()->willReturn('abc');
        $this->shouldThrow('\Exception')->duringValidate($parameters);
    }
}
