<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\Validator;

use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use PhpSpec\ObjectBehavior;

class CurrencyValidatorSpec extends ObjectBehavior
{
    function it_should_implement_query_parameter_validator_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\QueryParameterValidator');
    }

    function it_should_check_both_given_currency_type_and_length_correct(
        Parameters $parameters
    ) {
        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn('USD');
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn('GBP');

        $this->validate($parameters);
    }

    function it_should_check_only_from_currency_type_and_length_correct_when_to_currency_is_missing(
        Parameters $parameters
    ) {
        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn('USD');
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn(null);

        $this->validate($parameters);
    }

    function it_should_throw_an_exception_when_from_currency_character_limit_not_equal_to_three_or_non_string(
        Parameters $parameters
    ) {
        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn(null);
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn(null);

        $this->shouldThrow('\Exception')->duringValidate($parameters);

        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn('AB');
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn(null);

        $this->shouldThrow('\Exception')->duringValidate($parameters);

        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn(100);
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn(null);

        $this->shouldThrow('\Exception')->duringValidate($parameters);

        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn('ABC');
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn(100);

        $this->shouldThrow('\Exception')->duringValidate($parameters);

        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn('ABCC');
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn('IOP');

        $this->shouldThrow('\Exception')->duringValidate($parameters);

        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn('ABC');
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn('IOPD');

        $this->shouldThrow('\Exception')->duringValidate($parameters);
    }
}
