<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\ValueObject;

use PhpSpec\ObjectBehavior;

class MessageSpec extends ObjectBehavior
{
    /** @var array */
    private $sender = array('sender' => 'nr@mvfglobal.com');

    /** @var array */
    private $recipient = array('kj@mvfglobal.com');

    /** @var string */
    private $subject = 'Test Email Subject';

    /** @var array */
    private $body = 'Test Body';

    function let()
    {
        $this->beConstructedWith($this->subject, $this->body, $this->sender, $this->recipient);
    }

    function it_should_let_you_get_senders_details()
    {
        $this->getSender()->shouldReturn($this->sender);
    }

    function it_should_let_you_get_recipient_detail()
    {
        $this->getRecipient()->shouldReturn($this->recipient);
    }

    function it_should_let_you_get_subject()
    {
        $this->getSubject()->shouldReturn($this->subject);
    }

    function it_should_let_you_get_message_body()
    {
        $this->getMessageBody()->shouldReturn($this->body);
    }
}
