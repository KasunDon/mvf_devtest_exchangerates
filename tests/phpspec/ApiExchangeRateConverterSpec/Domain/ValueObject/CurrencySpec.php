<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\ValueObject;

use PhpSpec\ObjectBehavior;

class CurrencySpec extends ObjectBehavior
{
    /** @var string */
    private $currency = 'GBP';

    function let()
    {
        $this->beConstructedWith($this->currency);
    }

    function it_should_let_you_get_currency_value()
    {
        $this->getValue()->shouldReturn($this->currency);
    }

    function it_should_make_input_currency_uppercase_when_lowercase_given()
    {
        $this->beConstructedWith('gbp');
        $this->getValue()->shouldReturn($this->currency);
    }

    function it_should_throw_an_exception_given_invalid_currency()
    {
        $this->beConstructedWith(0);
        $this->shouldThrow('\InvalidArgumentException')->duringInstantiation();
    }

    function it_should_throw_an_exception_given_empty_currency()
    {
        $this->beConstructedWith(null);
        $this->shouldThrow('\InvalidArgumentException')->duringInstantiation();
    }
}
