<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\ValueObject;

use PhpSpec\ObjectBehavior;

class ParametersSpec extends ObjectBehavior
{
    /** @var array */
    private $payload = array(
        'param1' => '1',
        'param2' => '2',
        'param3' => '3',
        'param4' => '4'
    );

    public function let()
    {
        $this->beConstructedWith($this->payload);
    }

    public function it_should_let_you_access_parameters_via_get_method()
    {
        $this->get('param1')->shouldReturn($this->payload['param1']);
        $this->get('param2')->shouldReturn($this->payload['param2']);
        $this->get('param3')->shouldReturn($this->payload['param3']);
        $this->get('param4')->shouldReturn($this->payload['param4']);
    }

    public function it_should_return_null_when_param_name_not_found()
    {
        $this->get('param123')->shouldReturn(null);
    }
}
