<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Domain\ValueObject;

use PhpSpec\ObjectBehavior;

class AmountSpec extends ObjectBehavior
{
    /** @var int */
    private $amount = 100.00;

    public function it_should_let_you_get_amount_value()
    {
        $this->beConstructedWith($this->amount);
        $this->getValue()->shouldBe($this->amount);
    }

    public function it_should_throw_an_exception_if_given_amount_not_a_number()
    {
        $nonInteger = 'a123';

        $this->shouldThrow('\InvalidArgumentException')
            ->during('__construct', array($nonInteger));
    }
}
