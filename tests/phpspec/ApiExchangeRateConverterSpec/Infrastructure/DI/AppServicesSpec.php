<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\DI;

use PhpSpec\ObjectBehavior;

class AppServicesSpec extends ObjectBehavior
{

    function it_should_let_you_get_container()
    {
        $this->getContainer()->shouldHaveType('Symfony\Component\DependencyInjection\ContainerBuilder');
    }

    function it_should_allow_to_load_service_definition_file_into_container()
    {
        $this->loadFromDefinitionFilePath();
    }


    function it_should_throw_an_exception_when_service_definition_cannot_be_located()
    {
        $serviceFile = 'services/abc.yml';

        $this->shouldThrow('Symfony\Component\Config\Exception\FileLocatorFileNotFoundException')
            ->duringLoadFromDefinitionFilePath($serviceFile);
    }
}
