<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Mail;

use MVF\ApiExchangeRateConverter\Domain\ValueObject\Message;
use PhpSpec\ObjectBehavior;

class MailAdapterSpec extends ObjectBehavior
{
    function it_should_implement_mailer_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\MailerInterface');
    }

    function let(\Swift_Mailer $mailer)
    {
        $this->beConstructedWith($mailer);
    }

    function it_should_let_you_send_message_via_mail_adapter(
        \Swift_Mailer $mailer,
        \Swift_Message $swiftMessage,
        Message $message
    ) {
        $body = 'internal-notification';
        $sender = array('Awin' => 'nr@mvfglobal.com');
        $recipient = array('kj@mvfglobal.com');
        $subject = 'Test Email';

        $message->getMessageBody()->shouldBeCalled()->willReturn($body);
        $message->getSender()->shouldBeCalled()->willReturn($sender);
        $message->getRecipient()->shouldBeCalled()->willReturn($recipient);
        $message->getSubject()->shouldBeCalled()->willReturn($subject);

        $mailer->createMessage()->shouldBeCalled()->willReturn($swiftMessage);

        $swiftMessage->setSubject($subject)->shouldBeCalled()->willReturn($swiftMessage);
        $swiftMessage->setFrom($sender)->shouldBeCalled()->willReturn($swiftMessage);
        $swiftMessage->setTo($recipient)->shouldBeCalled()->willReturn($swiftMessage);
        $swiftMessage->setBody($body)->shouldBeCalled()->willReturn($swiftMessage);

        $mailer->send($swiftMessage)->shouldBeCalled();

        $this->send($message);
    }
}
