<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Config;

use MVF\ApiExchangeRateConverter\Infrastructure\Config\ConfigParameter;
use PhpSpec\ObjectBehavior;

class ConfigParameterSpec extends ObjectBehavior
{
    /** @var array */
    private $config = array(
        'code' => array(
            'run' => 'go',
            'db' => array(
                'adapter' => array(
                    'name' => 'test'
                )
            )
        ),
        'test' => 'test-live'
    );

    function it_should_implement_configurator_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface');
    }

    function let()
    {
        $this->beConstructedWith($this->config);
    }

    function it_should_wrap_within_config_parameter_obj_when_end_value_is_an_array()
    {
        $key = 'code';
        $expectedValue = new ConfigParameter($this->config[$key]);
        $this->get($key)->shouldBeLike($expectedValue);
    }

    function it_should_return_raw_value_when_end_value_is_not_an_array()
    {
        $key = 'test';
        $expectedValue = $this->config[$key];
        $this->get($key)->shouldReturn($expectedValue);
    }

    function it_lets_you_get_child_config_values_from_dot_delimited_index_location()
    {
        $key = 'code.run';
        $expectedValue = $this->config['code']['run'];

        $this->get($key)->shouldReturn($expectedValue);


        $keyWithTrailingDot = 'code.';
        $expectedValue = new ConfigParameter($this->config['code']);
        $this->get($keyWithTrailingDot)->shouldBeLike($expectedValue);

        $deepIndexes = 'code.db.adapter.name';
        $expectedValue = $this->config['code']['db']['adapter']['name'];
        $this->get($deepIndexes)->shouldBeLike($expectedValue);
    }

    function it_should_return_config_as_array_when_call_to_array()
    {
        $this->toArray()->shouldReturn($this->config);
    }
}
