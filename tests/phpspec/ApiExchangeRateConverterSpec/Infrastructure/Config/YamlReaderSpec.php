<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Config;

use PhpSpec\ObjectBehavior;
use Symfony\Component\Yaml\Parser;

class YamlReaderSpec extends ObjectBehavior
{
    function it_should_implement_config_reader_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\ConfigReaderInterface');
    }

    function let(Parser $parser)
    {
        $this->beConstructedWith($parser);
    }

    function it_should_read_app_config_file_from_config_folder(
        Parser $parser
    ) {
        $config = array('common' => array('test'));

        $parser->parse(file_get_contents(realpath(APPLICATION_ROOT_DIR . "config/app.yml")))
            ->shouldBeCalled()
            ->willReturn($config);

        $this->read()->shouldReturn($config);
    }
}
