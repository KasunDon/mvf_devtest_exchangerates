<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Config;

use MVF\ApiExchangeRateConverter\Domain\ConfigReaderInterface;
use MVF\ApiExchangeRateConverter\Infrastructure\Config\ConfigParameter;
use PhpSpec\ObjectBehavior;

class ConfigBuilderSpec extends ObjectBehavior
{
    function let(ConfigReaderInterface $configReader)
    {
        $this->beConstructedWith($configReader);
    }

    function it_should_let_you_get_config_by_calling_get_config(
        ConfigReaderInterface $configReader
    ) {
        $config = array(
            'code' => array(
                'run' => 'go',
                'db' => array(
                    'adapter' => array(
                        'name' => 'test'
                    )
                )
            ),
            'test' => 'test-live'
        );

        $configReader->read()->shouldBeCalled()->willReturn($config);

        $expectedValue = new ConfigParameter($config);
        $this->build()->shouldBeLike($expectedValue);
    }
}
