<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Http;

use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;
use MVF\ApiExchangeRateConverter\Domain\HTTPClientProviderInterface;
use PhpSpec\ObjectBehavior;

class RestfulExchangeRateFetcherSpec extends ObjectBehavior
{
    function it_should_implement_exchange_rate_fetcher_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\ExchangeRateFetcherInterface');
    }

    function let(ConfiguratorInterface $configurator, HTTPClientProviderInterface $httpProvider)
    {
        $this->beConstructedWith($configurator, $httpProvider);
    }

    function it_should_fetch_exchange_rates_from_rest_api_and_return_an_array(
        ConfiguratorInterface $configurator,
        HTTPClientProviderInterface $httpProvider
    ) {
        $url = 'http://api.google.com/?json';
        $appId = 'afdsfsdf234354rfsd';
        $configurator->get('parameters.exchange_rate_api.url')->shouldBeCalled()->willReturn($url);
        $configurator->get('parameters.exchange_rate_api.appId')
            ->shouldBeCalled()
            ->willReturn($appId);

        $queryParams = array('app_id' => $appId);
        $output = '{
                      "disclaimer": "Usage subject to terms: https://openexchangerates.org/terms",
                      "license": "https://openexchangerates.org/license",
                      "timestamp": 1494165627,
                      "base": "USD",
                      "rates": {
                        "AED": 3.672779,
                        "AFN": 67.3485,
                        "ALL": 122.740964,
                        "AMD": 482.494884,
                        "ANG": 1.773448,
                        "AOA": 165.9125,
                        "ARS": 15.352333
                      }
                    }';

        $expectedOutput = array(
            'AED' => 3.672779,
            'AFN' => 67.3485,
            'ALL' => 122.740964,
            'AMD' => 482.494884,
            'ANG' => 1.773448,
            'AOA' => 165.9125,
            'ARS' => 15.352333
        );

        $httpProvider->get($url, $queryParams)->shouldBeCalled()->willReturn($output);

        $this->fetch()->shouldReturn($expectedOutput);
    }
}
