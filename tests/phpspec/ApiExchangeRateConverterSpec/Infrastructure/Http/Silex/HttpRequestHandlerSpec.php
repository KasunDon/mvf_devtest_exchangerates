<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Http\Silex;

use MVF\ApiExchangeRateConverter\Domain\QueryParameterValidator;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\Request;

class HttpRequestHandlerSpec extends ObjectBehavior
{
    function it_should_implement_http_request_handler_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Infrastructure\Http\HttpRequestHandlerInterface');
    }

    function let(QueryParameterValidator $parameterValidator)
    {
        $this->beConstructedWith(array($parameterValidator));
    }

    function it_should_handle_requests_through_http_framework()
    {
        $requestInput = array(
            Parameters::AMOUNT => 100,
            Parameters::FROM_CURRENCY => 'USD',
            Parameters::TO_CURRENCY => 'JPY'
        );

        $httpRequest = new Request(array(), $requestInput);

        $this->handle($httpRequest)->shouldHaveType('MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters');
    }
}
