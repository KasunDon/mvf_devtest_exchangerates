<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\Http;

use GuzzleHttp\Client;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class HttpClientSpec extends ObjectBehavior
{
    function it_should_implement_http_client_provider_interface()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\HTTPClientProviderInterface');
    }

    function let(Client $client)
    {
        $this->beConstructedWith($client);
    }

    function it_should_let_you_make_GET_request_for_given_url(
        Client $client,
        ResponseInterface $response,
        StreamInterface $streamInterface
    ) {
        $url = 'http://abc.google.com';
        $query = array();
        $expectedValue = "abcd";

        $client->request('GET', $url, array('query' => $query))->willReturn($response);
        $response->getBody()->shouldBeCalled()->willReturn($streamInterface);
        $streamInterface->getContents()->shouldBeCalled()->willReturn($expectedValue);
        $this->get($url, $query)->shouldBe($expectedValue);
    }
}
