<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\DataStorage;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use PhpSpec\ObjectBehavior;

class SQLiteExchangeRateRepositorySpec extends ObjectBehavior
{
    function it_should_implement_exchange_rate_repository()
    {
        $this->shouldImplement('MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository');
    }

    function let(EntityManager $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_should_let_you_save_not_peristed_entity(
        ExchangeRate $exchangeRate,
        EntityManager $entityManager
    ) {
        $entityManager->contains($exchangeRate)->shouldBeCalled()->willReturn(false);

        $entityManager->persist($exchangeRate)->shouldBeCalled();

        $entityManager->flush($exchangeRate)->shouldBeCalled();

        $this->save($exchangeRate);
    }


    function it_should_let_you_save_any_update_made_to_persisting_entity(
        ExchangeRate $exchangeRate,
        EntityManager $entityManager
    ) {
        $entityManager->contains($exchangeRate)->shouldBeCalled()->willReturn(true);

        $entityManager->persist($exchangeRate)->shouldNotBeCalled();

        $entityManager->flush($exchangeRate)->shouldBeCalled();

        $this->save($exchangeRate);
    }

    function it_should_let_you_find_exchange_rate_by_currency(
        EntityManager $entityManager,
        EntityRepository $entityRepository,
        Currency $currency,
        ExchangeRate $exchangeRate
    ) {
        $currency->getValue()->shouldBeCalled()->willReturn('GBP');

        $exchangeRatesRepository = $entityManager->getRepository('\MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate')
            ->shouldBeCalled()
            ->willReturn($entityRepository);

        $entityRepository->findOneBy(array('currency' => 'GBP'))->shouldBeCalled()->willReturn($exchangeRate);

        $this->findByCurrency($currency)->shouldHaveType('MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate');
    }

    function it_should_return_false_when_no_find_exchange_rate_by_currency(
        EntityManager $entityManager,
        EntityRepository $entityRepository,
        Currency $currency,
        ExchangeRate $exchangeRate
    ) {
        $currency->getValue()->shouldBeCalled()->willReturn('GBP');

        $entityManager->getRepository('\MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate')
            ->shouldBeCalled()
            ->willReturn($entityRepository);

        $entityRepository->findOneBy(array('currency' => 'GBP'))->shouldBeCalled()->willReturn(null);

        $this->findByCurrency($currency)->shouldReturn(false);
    }
}
