<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Infrastructure\DataStorage\Doctrine;

use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;
use PhpSpec\ObjectBehavior;

class EntityManagerFactorySpec extends ObjectBehavior
{
    function let(ConfiguratorInterface $configurator)
    {
        $this->beConstructedWith($configurator);
    }

    function it_should_let_you_create_entity_manager_instance(
        ConfiguratorInterface $configurator
    ) {
        $expectedConnectionSettings = array(
            'dbname' => 'exchange_rates',
            'user' => 'user',
            'host' => 'localhost',
            'password' => '123',
            'driver' => 'pdo_sqlite'
        );

        $configurator->get('parameters.database.connection')
            ->shouldBeCalled()
            ->willReturn($configurator);

        $configurator->toArray()
            ->shouldBeCalled()
            ->willReturn($expectedConnectionSettings);

        $configurator->get('parameters.database.mapping')
            ->shouldBeCalled()
            ->willReturn($configurator);

        $this->create()->shouldHaveType('Doctrine\ORM\EntityManager');
    }
}
