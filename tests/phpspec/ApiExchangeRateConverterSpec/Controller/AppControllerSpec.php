<?php

namespace ApiExchangeRateConverterSpec\MVF\ApiExchangeRateConverter\Controller;

use MVF\ApiExchangeRateConverter\Domain\MailerInterface;
use MVF\ApiExchangeRateConverter\Domain\Model\ExchangeRateConverter;
use MVF\ApiExchangeRateConverter\Domain\Model\ExchangeRateUpdater;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Amount;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Message;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use PhpSpec\ObjectBehavior;

class AppControllerSpec extends ObjectBehavior
{
    function let(
        ExchangeRateUpdater $exchangeRateUpdater,
        ExchangeRateConverter $exchangeRateConverter,
        MailerInterface $mailer
    ) {
        $this->beConstructedWith($exchangeRateUpdater, $exchangeRateConverter, $mailer);
    }

    function it_should_return_an_array_output_for_index_action()
    {
        $this->indexAction()->shouldReturn(array('test' => 'index'));
    }

    function it_should_return_ok_output_for_update_exchange_rates_action(
        ExchangeRateUpdater $exchangeRateUpdater
    ) {
        $exchangeRateUpdater->update()->shouldBeCalled();
        $this->updateExchangeRateAction()->shouldReturn(array('status' => 'OK'));
    }

    function it_should_return_failed_output_for_update_exchange_rates_action_and_should_send_a_email(
        ExchangeRateUpdater $exchangeRateUpdater,
        MailerInterface $mailer
    ) {
        $exchangeRateUpdater->update()->shouldBeCalled()->willThrow('\Exception');

        $message = new Message(
            'CRITICAL ERROR: Exchange rates update unsuccessful',
            'Exchange rates update unsuccessful from openexchangerates.org.',
            array('app-error@mvfglobal.com'),
            array('admin@mvfglobal.com')
        );

        $mailer->send($message)->shouldBeCalled();

        $this->updateExchangeRateAction()->shouldReturn(array('status' => 'FAILED'));
    }

    function it_should_return_an_array_output_for_convert_currency_action(
        ExchangeRateConverter $exchangeRateConverter,
        Parameters $parameters
    ) {
        $amount = new Amount(100);
        $originalCurrency = new Currency('USD');
        $desiredCurrency = new Currency('GBP');

        $parameters->get(Parameters::FROM_CURRENCY)->shouldBeCalled()->willReturn($originalCurrency->getValue());
        $parameters->get(Parameters::TO_CURRENCY)->shouldBeCalled()->willReturn($desiredCurrency->getValue());
        $parameters->get(Parameters::AMOUNT)->shouldBeCalled()->willReturn($amount->getValue());

        $expectedOutput = array(
            Parameters::FROM_CURRENCY => $originalCurrency->getValue(),
            Parameters::TO_CURRENCY => $desiredCurrency->getValue(),
            Parameters::EXCHANGE_RATE => 0.776165,
            Parameters::ORIGINAL_AMOUNT => $amount->getValue(),
            Parameters::CONVERTED_AMOUNT => 77.6165

        );

        $exchangeRateConverter->convert($amount, $originalCurrency, $desiredCurrency)
            ->shouldBeCalled()
            ->willReturn($expectedOutput);

        $this->convertCurrencyAction($parameters)->shouldReturn($expectedOutput);
    }
}
