<?php

namespace Tests;

class Bootstrap
{
    public function run()
    {
        require_once __DIR__ . '/../vendor/autoload.php';
        define('APPLICATION_ROOT_DIR', __DIR__ . '/../');
    }
}

$bootstrap = new Bootstrap();
$bootstrap->run();
