<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Mail;

use MVF\ApiExchangeRateConverter\Domain\MailerInterface;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Message;
use Swift_Mailer;

class MailAdapter implements MailerInterface
{
    /** @var Swift_Mailer */
    private $mailer;

    /**
     * @param Swift_Mailer $mailer
     */
    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Message $message
     * @return void
     */
    public function send(Message $message)
    {
        $message = $this->mailer->createMessage()
            ->setSubject($message->getSubject())
            ->setFrom($message->getSender())
            ->setTo($message->getRecipient())
            ->setBody($message->getMessageBody());

        $this->mailer->send($message);
    }
}
