<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Config;

use MVF\ApiExchangeRateConverter\Domain\ConfigReaderInterface;

class ConfigBuilder
{
    /** @var ConfigReaderInterface */
    private $configReader;

    /**
     * @param ConfigReaderInterface $configReader
     */
    public function __construct(ConfigReaderInterface $configReader)
    {
        $this->configReader = $configReader;
    }

    /**
     * @return ConfigParameter
     */
    public function build()
    {
        return new ConfigParameter($this->configReader->read());
    }
}
