<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Config;

use MVF\ApiExchangeRateConverter\Domain\ConfigReaderInterface;
use Symfony\Component\Yaml\Parser;

class YamlReader implements ConfigReaderInterface
{
    /** @var Parser */
    private $parser;

    /**
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @return array
     */
    public function read()
    {
        $path = realpath(APPLICATION_ROOT_DIR . 'config/app.yml');
        return $this->parser->parse(file_get_contents($path));
    }
}
