<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Config;

use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;

class ConfigParameter implements ConfiguratorInterface
{
    /** @var array */
    private $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $config
     * @return mixed
     */
    public function get($config)
    {
        $value = $this->findConfig($config);

        if (is_array($value)) {
            return new self($value);
        }

        return $value;
    }

    /**
     * @param string $config
     *
     * @return array|string
     */
    protected function findConfig($config)
    {
        $index = array_filter(explode('.', $config));

        $returnValue = $this->config;

        foreach ($index as $value) {
            $returnValue = $returnValue[$value];
        }

        return $returnValue;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->config;
    }
}
