<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Http;

use GuzzleHttp\Client as GuzzleClient;
use MVF\ApiExchangeRateConverter\Domain\HTTPClientProviderInterface;

class HttpClient implements HTTPClientProviderInterface
{
    /** @var GuzzleClient */
    private $guzzleClient;

    /**
     * @param GuzzleClient $guzzleClient
     */
    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param string $url
     * @param array $queryParams
     *
     * @return string
     */
    public function get($url, $queryParams)
    {
        return $this->guzzleClient
            ->request('GET', $url, array('query' => $queryParams))
            ->getBody()
            ->getContents();
    }
}
