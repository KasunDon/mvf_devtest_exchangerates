<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Http\Silex;

use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use MVF\ApiExchangeRateConverter\Infrastructure\Http\HttpRequestHandlerInterface;

class HttpRequestHandler implements HttpRequestHandlerInterface
{
    /** @var array */
    private $queryValidators;

    /**
     * @param array $queryValidators
     */
    public function __construct(array $queryValidators)
    {
        $this->queryValidators = $queryValidators;
    }

    public function handle($request)
    {
        $parameters = new Parameters($request->request->all());

        $this->runQueryValidators($parameters);

        return $parameters;
    }

    /**
     * @param Parameters $parameters
     */
    private function runQueryValidators(Parameters $parameters)
    {
        foreach ($this->queryValidators as $validator) {
            $validator->validate($parameters);
        }
    }
}
