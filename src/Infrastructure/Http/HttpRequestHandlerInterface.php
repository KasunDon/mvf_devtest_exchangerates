<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Http;

interface HttpRequestHandlerInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function handle($request);
}
