<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\Http;

use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateFetcherInterface;
use MVF\ApiExchangeRateConverter\Domain\HTTPClientProviderInterface;

class RestfulExchangeRateFetcher implements ExchangeRateFetcherInterface
{
    /** @var ConfiguratorInterface */
    private $configurator;

    /** @var HTTPClientProviderInterface */
    private $httpProvider;

    /**
     * @param ConfiguratorInterface $configurator
     * @param HTTPClientProviderInterface $httpProvider
     */
    public function __construct(ConfiguratorInterface $configurator, HTTPClientProviderInterface $httpProvider)
    {
        $this->configurator = $configurator;
        $this->httpProvider = $httpProvider;
    }

    /**
     * @return array
     */
    public function fetch()
    {
        $endpoint = $this->configurator->get('parameters.exchange_rate_api.url');
        $appId = $this->configurator->get('parameters.exchange_rate_api.appId');

        $queryParams = array('app_id' => $appId);

        $httpOutput = $this->httpProvider->get($endpoint, $queryParams);
        $result = json_decode($httpOutput, true);

        return $result['rates'];
    }
}
