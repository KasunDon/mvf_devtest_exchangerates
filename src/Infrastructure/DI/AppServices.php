<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\DI;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class AppServices
{
    /** @var AppServices */
    private static $selfInstance;
    /** @var ContainerBuilder */
    private $containerBuilder;

    /**
     * AppServices constructor.
     */
    public function __construct()
    {
        $this->containerBuilder = new ContainerBuilder();
    }

    /**
     * @return AppServices
     */
    public static function initialize()
    {
        if (!self::$selfInstance) {
            self::$selfInstance = new self();
        }

        return self::$selfInstance;
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->containerBuilder;
    }

    /**
     * @param string|null $definitionPath
     */
    public function loadFromDefinitionFilePath($definitionPath = null)
    {
        $serviceFilePath = empty($definitionPath) ?
            realpath(APPLICATION_ROOT_DIR . 'config/services.yml') : $definitionPath;

        $loader = new YamlFileLoader($this->containerBuilder, new FileLocator(dirname($serviceFilePath)));
        $loader->load(basename($serviceFilePath));
    }
}
