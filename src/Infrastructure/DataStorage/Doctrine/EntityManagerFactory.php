<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\DataStorage\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\ORM\Tools\Setup;
use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;

class EntityManagerFactory
{
    /** @var ConfiguratorInterface */
    private $configurator;

    /**
     * @param ConfiguratorInterface $configurator
     */
    public function __construct(ConfiguratorInterface $configurator)
    {
        $this->configurator = $configurator;
    }

    /**
     * @return EntityManager
     */
    public function create()
    {
        $connection = $this->configurator->get('parameters.database.connection')->toArray();
        $mapping = $this->configurator->get('parameters.database.mapping')->toArray();

        return EntityManager::create($connection, $this->getDoctrineConfiguration($mapping));
    }

    /**
     * @param array $mapping
     * @return \Doctrine\ORM\Configuration
     */
    private function getDoctrineConfiguration($mapping)
    {
        $setup = Setup::createXMLMetadataConfiguration($mapping, false);
        $setup->setMetadataDriverImpl(new XmlDriver($mapping));

        return $setup;
    }
}
