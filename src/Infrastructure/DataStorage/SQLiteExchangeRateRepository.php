<?php

namespace MVF\ApiExchangeRateConverter\Infrastructure\DataStorage;

use Doctrine\ORM\EntityManager;
use MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;

class SQLiteExchangeRateRepository implements ExchangeRateRepository
{
    /** @var EntityManager */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ExchangeRate $exchangeRate
     * @return void
     */
    public function save(ExchangeRate $exchangeRate)
    {
        if (!$this->entityManager->contains($exchangeRate)) {
            $this->entityManager->persist($exchangeRate);
        }

        $this->entityManager->flush($exchangeRate);
    }

    /**
     * @return array
     */
    public function fetchAllCurrencyAndRate()
    {

        $statement = $this->entityManager
            ->getConnection()
            ->prepare('SELECT currency, rate FROM exchange_rates');

        $statement->execute();

        return array_reduce($statement->fetchAll(), function ($carry, $item) {
            $carry[$item['currency']] = $item['rate'];

            return $carry;
        });
    }

    /**
     * @param Currency $currency
     * @return mixed [ExchangeRate|false]
     */
    public function findByCurrency(Currency $currency)
    {
        $exchangeRate = $this->entityManager
            ->getRepository('\MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate')
            ->findOneBy(array('currency' => $currency->getValue()));

        if (!$exchangeRate) {
            return false;
        }

        return $exchangeRate;
    }
}
