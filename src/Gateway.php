<?php

namespace MVF\ApiExchangeRateConverter;

use Monolog\Logger;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;
use MVF\ApiExchangeRateConverter\Infrastructure\DI\AppServices;
use Silex\Application;
use Silex\Provider\MonologServiceProvider;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;

class Gateway
{
    /** @var Application */
    private $application;

    /** @var ContainerBuilder */
    private $container;

    /**
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * @param Application $application
     */
    public static function bootstrap(Application $application)
    {
        $bootstrap = new self($application);
        $bootstrap->run();
    }

    /**
     * @param bool $debug
     */
    public function run($debug = false)
    {
        $this->application['debug'] = $debug;
        $this->setupErrorHandler();
        $this->initializeServices();
        $this->initializeRoutes();
        $this->application->run();
    }

    /**
     * Error handling
     */
    public function setupErrorHandler()
    {
        $this->application->register(new MonologServiceProvider(), [
            'monolog.logfile' => '/var/log/exchangerate-app-error.log',
            'monolog.level' => Logger::ERROR
        ]);

        $this->application->error(function (\Exception $e, Request $request, $errorCode) {
            $message = array('error' => $e->getMessage());
            return $this->application->json($message, $errorCode);
        });
    }

    /**
     * @return void
     */
    public function initializeServices()
    {
        $appServices = AppServices::initialize();
        $appServices->loadFromDefinitionFilePath();
        $this->container = $appServices->getContainer();
    }

    /**
     * @return void
     */
    public function initializeRoutes()
    {
        $appController = $this->container->get('api_exchange_rate_converter_app_controller');

        $this->application->get('/test',
            function (Request $request) use ($appController) {
                return $this->application->json($appController->indexAction());
            }
        );

        $this->application->get('/test/update_exchange_rates',
            function (Request $request) use ($appController) {
                return $this->application->json($appController->updateExchangeRateAction());
            }
        );

        $this->application->get('/test/convert_currency/{amount}/{from_currency}/{to_currency}',
            $this->getCurrencyConversionCallback($appController)
        )->before($this->getRequestHandlerCallback());

        $this->application->get('/test/convert_currency/{amount}/{from_currency}',
            $this->getCurrencyConversionCallback($appController)
        )->before($this->getRequestHandlerCallback());
    }

    /**
     * @param $appController
     * @return \Closure
     */
    private function getCurrencyConversionCallback($appController)
    {
        return function (Request $request) use ($appController) {
            return $this->application
                ->json(
                    $appController
                        ->convertCurrencyAction($request->get('queryParameters'))
                );
        };
    }

    /**
     * @return \Closure
     */
    private function getRequestHandlerCallback()
    {
        $httpRequestHandler = $this->container->get('api_exchange_rate_converter_http_request_handler');

        return function (Request $request) use ($httpRequestHandler) {

            $request->request->set(Parameters::AMOUNT, $request->attributes->get(Parameters::AMOUNT));
            $request->request->set(Parameters::FROM_CURRENCY, $request->attributes->get(Parameters::FROM_CURRENCY));
            $request->request->set(Parameters::TO_CURRENCY, $request->attributes->get(Parameters::TO_CURRENCY));

            $queryParameters = $httpRequestHandler->handle($request);

            $request->attributes->set('queryParameters', $queryParameters);
        };
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @return ContainerBuilder
     */
    public function getServiceContainer()
    {
        return $this->container;
    }
}
