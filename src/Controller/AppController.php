<?php

namespace MVF\ApiExchangeRateConverter\Controller;

use MVF\ApiExchangeRateConverter\Domain\MailerInterface;
use MVF\ApiExchangeRateConverter\Domain\Model\ExchangeRateConverter;
use MVF\ApiExchangeRateConverter\Domain\Model\ExchangeRateUpdater;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Amount;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Message;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;

class AppController
{
    /** @var ExchangeRateUpdater */
    private $exchangeRateUpdater;

    /** @var ExchangeRateConverter */
    private $exchangeRateConverter;

    /** @var MailerInterface */
    private $mailerProvider;

    /**
     * @param ExchangeRateUpdater $exchangeRateUpdater
     * @param ExchangeRateConverter $exchangeRateConverter
     * @param MailerInterface $mailerProvider
     */
    public function __construct(
        ExchangeRateUpdater $exchangeRateUpdater,
        ExchangeRateConverter $exchangeRateConverter,
        MailerInterface $mailerProvider
    ) {
        $this->exchangeRateUpdater = $exchangeRateUpdater;
        $this->exchangeRateConverter = $exchangeRateConverter;
        $this->mailerProvider = $mailerProvider;
    }

    /**
     * Index Action
     *
     * @return array
     */
    public function indexAction()
    {
        return array('test' => 'index');
    }

    /**
     * Update Exchange Rate Action
     *
     * @return array
     */
    public function updateExchangeRateAction()
    {
        $status = 'FAILED';

        try {
            $this->exchangeRateUpdater->update();
            $status = 'OK';
        } catch (\Exception $e) {
            $this->mailerProvider
                ->send(
                    $this->getFailureMessage()
                );
        }

        return array('status' => $status);
    }

    /**
     * @return Message
     */
    private function getFailureMessage()
    {
        return new Message(
            'CRITICAL ERROR: Exchange rates update unsuccessful',
            'Exchange rates update unsuccessful from openexchangerates.org.',
            array('app-error@mvfglobal.com'),
            array('admin@mvfglobal.com')
        );
    }

    /**
     * @param Parameters $parameters
     * @return array
     */
    public function convertCurrencyAction(Parameters $parameters)
    {
        $amount = new Amount($parameters->get(Parameters::AMOUNT));
        $originalCurrency = new Currency($parameters->get(Parameters::FROM_CURRENCY));
        $toCurrency = $parameters->get(Parameters::TO_CURRENCY);

        if (isset($toCurrency)) {
            $desiredCurrency = new Currency($parameters->get(Parameters::TO_CURRENCY));

            return $this->exchangeRateConverter
                ->convert($amount, $originalCurrency, $desiredCurrency);
        }

        return $this->exchangeRateConverter
            ->convert($amount, $originalCurrency);
    }
}
