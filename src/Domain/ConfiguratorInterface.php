<?php

namespace MVF\ApiExchangeRateConverter\Domain;

interface ConfiguratorInterface
{
    /**
     * @param string $config
     * @return mixed
     */
    public function get($config);


    /**
     * @return array
     */
    public function toArray();
}
