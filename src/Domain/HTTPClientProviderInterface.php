<?php

namespace MVF\ApiExchangeRateConverter\Domain;

interface HTTPClientProviderInterface
{
    /**
     * @param $url
     * @param $queryParams
     *
     * @return mixed
     */
    public function get($url, $queryParams);
}
