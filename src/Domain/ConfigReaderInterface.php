<?php

namespace MVF\ApiExchangeRateConverter\Domain;

interface ConfigReaderInterface
{
    /**
     * @return array
     */
    public function read();
}
