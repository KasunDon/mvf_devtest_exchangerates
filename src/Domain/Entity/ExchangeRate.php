<?php

namespace MVF\ApiExchangeRateConverter\Domain\Entity;

class ExchangeRate
{
    /*** @var float */
    private $rate;

    /*** @var \DateTime */
    private $updated;

    /*** @var string */
    private $currency;

    /**
     * Get rate
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set rate
     *
     * @param float $rate
     *
     * @return ExchangeRate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return ExchangeRate
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set Currency
     *
     * @param string $currency
     *
     * @return ExchangeRate
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }
}
