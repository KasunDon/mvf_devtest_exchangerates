<?php

namespace MVF\ApiExchangeRateConverter\Domain;

use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;

interface QueryParameterValidator
{
    /**
     * @param Parameters $parameters
     * @return void
     */
    public function validate(Parameters $parameters);
}
