<?php

namespace MVF\ApiExchangeRateConverter\Domain\Model;

use MVF\ApiExchangeRateConverter\Domain\ConfiguratorInterface;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Amount;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;

class ExchangeRateConverter
{
    /** @var ExchangeRateRepository */
    private $exchangeRateRepository;

    /** @var ConfiguratorInterface */
    private $configurator;

    /**
     * @param ExchangeRateRepository $exchangeRateRepository
     * @param ConfiguratorInterface $configurator
     */
    public function __construct(ExchangeRateRepository $exchangeRateRepository, ConfiguratorInterface $configurator)
    {
        $this->exchangeRateRepository = $exchangeRateRepository;
        $this->configurator = $configurator;
    }

    /**
     * @param Amount $amount
     * @param Currency $originalCurrency
     * @param Currency $desiredCurrency
     * @return array
     */
    public function convert(Amount $amount, Currency $originalCurrency, Currency $desiredCurrency = null)
    {

        $desiredCurrency = $this->assignDefaultDesiredCurrencyAsGBP($desiredCurrency);

        $output = array(
            Parameters::FROM_CURRENCY => $originalCurrency->getValue(),
            Parameters::TO_CURRENCY => $desiredCurrency->getValue(),
            Parameters::EXCHANGE_RATE => 0,
            Parameters::ORIGINAL_AMOUNT => $amount->getValue(),
            Parameters::CONVERTED_AMOUNT => 0

        );

        if ($this->isAllowed($originalCurrency, $desiredCurrency)) {
            $rate = $this->calculateRate($originalCurrency, $desiredCurrency);
            $output[Parameters::EXCHANGE_RATE] = $rate;
            $output[Parameters::CONVERTED_AMOUNT] = $rate * $amount->getValue();
        }

        return $output;
    }

    /**
     * @param Currency $desiredCurrency
     * @return Currency
     */
    private function assignDefaultDesiredCurrencyAsGBP(Currency $desiredCurrency = null)
    {
        if (isset($desiredCurrency)) {
            return $desiredCurrency;
        }

        return new Currency('GBP');
    }

    /**
     * @param Currency $originalCurrency
     * @param Currency $desiredCurrency
     * @return bool
     */
    private function isAllowed(Currency $originalCurrency, Currency $desiredCurrency)
    {
        $excludedCurrencies = $this->configurator->get('parameters.excluded-currencies')->toArray();
        $allCurrencies = $this->exchangeRateRepository->fetchAllCurrencyAndRate();

        array_filter($excludedCurrencies,
            function ($item) use (&$allCurrencies) {
                unset($allCurrencies[$item]);
            }
        );

        $allowedCurrencies = array_keys($allCurrencies);

        return (in_array($originalCurrency->getValue(), $allowedCurrencies)) &&
            (in_array($desiredCurrency->getValue(), $allowedCurrencies));
    }

    /**
     * @param Currency $originalCurrency
     * @param Currency $desiredCurrency
     * @return float
     */
    private function calculateRate(Currency $originalCurrency, Currency $desiredCurrency)
    {
        $originalCurrencyExchangeRate = $this->exchangeRateRepository->findByCurrency($originalCurrency);


        if (empty($originalCurrencyExchangeRate)) {
            return 0;
        }


        $desiredCurrencyExchangeRate = $this->exchangeRateRepository->findByCurrency($desiredCurrency);


        return ($desiredCurrencyExchangeRate->getRate() / $originalCurrencyExchangeRate->getRate());
    }
}
