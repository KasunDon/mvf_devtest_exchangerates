<?php

namespace MVF\ApiExchangeRateConverter\Domain\Model;

use MVF\ApiExchangeRateConverter\Domain\ExchangeRateFetcherInterface;
use MVF\ApiExchangeRateConverter\Domain\ExchangeRateRepository;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;

class ExchangeRateUpdater
{
    /** @var ExchangeRateFetcherInterface */
    private $exchangeRateFetcher;

    /** @var ExchangeRateRepository */
    private $exchangeRepository;

    /**
     * @param ExchangeRateFetcherInterface $exchangeRateFetcher
     * @param ExchangeRateRepository $exchangeRateRepository
     */
    public function __construct(
        ExchangeRateFetcherInterface $exchangeRateFetcher,
        ExchangeRateRepository $exchangeRateRepository
    ) {
        $this->exchangeRateFetcher = $exchangeRateFetcher;
        $this->exchangeRepository = $exchangeRateRepository;
    }

    public function update()
    {
        $rawExchangeRates = $this->exchangeRateFetcher->fetch();
        $allExchangeRates = $this->exchangeRepository->fetchAllCurrencyAndRate();

        $updates = array_intersect(array_keys($rawExchangeRates), array_keys($allExchangeRates));

        if (empty($updates)) {
            throw new \Exception('No rates updates found to proceed.');
        }

        foreach ($updates as $currency) {
            $this->updateCurrency($currency, $rawExchangeRates[$currency]);
        }
    }

    /**
     * @param string $currency
     * @param float $rate
     */
    private function updateCurrency($currency, $rate)
    {
        if ($rate > 0) {
            $exchangeRate = $this->exchangeRepository->findByCurrency(new Currency($currency));
            $exchangeRate->setRate($rate);
            $exchangeRate->setUpdated(new \DateTime());

            $this->exchangeRepository->save($exchangeRate);
        }
    }
}
