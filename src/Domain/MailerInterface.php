<?php

namespace MVF\ApiExchangeRateConverter\Domain;

use MVF\ApiExchangeRateConverter\Domain\ValueObject\Message;

interface MailerInterface
{
    /**
     * @param Message $message
     * @return void
     */
    public function send(Message $message);
}
