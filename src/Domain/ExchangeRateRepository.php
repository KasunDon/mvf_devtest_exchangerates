<?php

namespace MVF\ApiExchangeRateConverter\Domain;

use MVF\ApiExchangeRateConverter\Domain\Entity\ExchangeRate;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Currency;

interface ExchangeRateRepository
{
    /**
     * @param ExchangeRate $exchangeRate
     * @return void
     */
    public function save(ExchangeRate $exchangeRate);

    /**
     * @param Currency $currency
     * @return ExchangeRate
     */
    public function findByCurrency(Currency $currency);

    /**
     * @return array
     */
    public function fetchAllCurrencyAndRate();
}
