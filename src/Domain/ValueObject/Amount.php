<?php

namespace MVF\ApiExchangeRateConverter\Domain\ValueObject;

class Amount
{
    /** @var float */
    private $amount;

    /**
     * @param float $amount
     */
    public function __construct($amount)
    {
        $this->amount = $amount;
        $this->validate();
    }

    /**
     * @throws \InvalidArgumentException
     * @return void
     */
    private function validate()
    {
        if (!is_numeric($this->amount)) {
            throw new \InvalidArgumentException("amount is not valid ({$this->amount})");
        }
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return floatval($this->amount);
    }
}
