<?php

namespace MVF\ApiExchangeRateConverter\Domain\ValueObject;

class Parameters
{
    const FROM_CURRENCY = 'from_currency';
    const TO_CURRENCY = 'to_currency';
    const AMOUNT = 'amount';
    const EXCHANGE_RATE = 'exchange_rate';
    const ORIGINAL_AMOUNT = 'original_amount';
    const CONVERTED_AMOUNT = 'converted_amount';

    /** @var array */
    private $parameters;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param string $paramName
     * @return mixed|null
     */
    public function get($paramName)
    {
        return isset($this->parameters[$paramName]) ?
            $this->parameters[$paramName] : null;
    }
}
