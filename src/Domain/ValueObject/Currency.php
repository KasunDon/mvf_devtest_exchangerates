<?php

namespace MVF\ApiExchangeRateConverter\Domain\ValueObject;

class Currency
{
    /** @var string */
    private $currency;

    /**
     * @param string $currency
     */
    public function __construct($currency)
    {
        $this->currency = strtoupper($currency);
        $this->validate();
    }

    /**
     * @throws \InvalidArgumentException
     * @return void
     */
    private function validate()
    {
        if (empty($this->currency) || !is_string($this->currency)) {
            throw new \InvalidArgumentException("currency is not valid ({$this->currency})");
        }
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->currency;
    }
}
