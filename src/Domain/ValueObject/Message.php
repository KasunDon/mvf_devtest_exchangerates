<?php

namespace MVF\ApiExchangeRateConverter\Domain\ValueObject;

class Message
{
    /** @var string */
    private $subject;

    /** @var string */
    private $body;

    /** @var array */
    private $sender;

    /** @var array */
    private $recipient;

    /**
     * @param string $subject
     * @param string $body
     * @param array $sender
     * @param array $recipient
     */
    public function __construct($subject, $body, array $sender, array $recipient)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->sender = $sender;
        $this->recipient = $recipient;
    }

    /**
     * @return array
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return array
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getMessageBody()
    {
        return $this->body;
    }
}
