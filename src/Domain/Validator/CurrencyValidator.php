<?php

namespace MVF\ApiExchangeRateConverter\Domain\Validator;

use MVF\ApiExchangeRateConverter\Domain\QueryParameterValidator;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;

class CurrencyValidator implements QueryParameterValidator
{
    /**
     * @param Parameters $parameters
     * @throws \Exception
     */
    public function validate(Parameters $parameters)
    {
        if (!$this->isAllowedTypeAndLength($parameters)) {
            throw new \Exception('Invalid currency or currency not supported.');
        }
    }

    /**
     * @param Parameters $parameters
     * @return bool
     */
    private function isAllowedTypeAndLength(Parameters $parameters)
    {
        $isAllowed = false;
        $fromCurrency = $parameters->get(Parameters::FROM_CURRENCY);
        $toCurrency = $parameters->get(Parameters::TO_CURRENCY);

        if (is_string($fromCurrency) && strlen($fromCurrency) === 3) {
            $isAllowed = true;

            if (isset($toCurrency)) {
                if (is_string($toCurrency) && strlen($toCurrency) === 3) {
                    $isAllowed = true;
                } else {
                    $isAllowed = false;
                }
            }
        }

        return $isAllowed;
    }
}
