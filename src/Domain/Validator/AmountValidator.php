<?php

namespace MVF\ApiExchangeRateConverter\Domain\Validator;

use MVF\ApiExchangeRateConverter\Domain\QueryParameterValidator;
use MVF\ApiExchangeRateConverter\Domain\ValueObject\Parameters;

class AmountValidator implements QueryParameterValidator
{
    /**
     * @param Parameters $parameters
     * @throws \Exception
     */
    public function validate(Parameters $parameters)
    {
        $amount = $parameters->get(Parameters::AMOUNT);

        if (empty($amount) || !is_numeric($amount)) {
            throw new \Exception('Invalid amount or given type not supported.');
        }
    }
}
