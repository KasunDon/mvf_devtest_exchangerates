<?php

namespace MVF\ApiExchangeRateConverter\Domain;

interface ExchangeRateFetcherInterface
{
    /**
     * @return array
     */
    public function fetch();
}
