# MVF Global - Developer Tests - ExchangeRates

Thanks for applying for a developer role at MVF. 

We have a simple project which we would like you to take a look at in your own time. 

You can spend as much or as little time on it as you wish. But we would expect to receive a response in a 3-4 days.

The project is a Codeigniter app written in PHP. It contains a SQLite database for simplicity. You will need PHP, SQLite and PDO to run it. The app contains a Model and Controller which contain functions to manage our exchange rate data. 

*Challenge*

How would you improve our code? Fork our repo and let us see your ideas! Things you could think about could be:

1. Adding tests
2. Changing framework
3. Tidying up code
4. Applying coding standards
5. Rewriting using design patterns


===========================================================
### ChangeLog 

* Dockerized Application Infrastructure to on demand startup.
 
    `make build start`
 
    It'll open up port `80` for this application. 
    
    ##### Please note that you'll need docker engine in order to use this on demand.
 
 * Added a Default Endpoint Integration test to preserve original behaviour.
 `make tests`
 
 * Most of the components being united tested using `phpspec` and `PHPUnit` Integration test added to maintain functionality.

 * Following `psr-1` and `psr-2` coding standard across this code base and introduced `psr-4` autoloader.
 
 * Re-written using `TDD` and `DDD` concepts to better testing and abstract infrastructure and domain components.
  
 * Used `Silex` as Http Framework, Can changed to any framework at later stage.
 
 * Notification setup being tested using `Swift_MemorySpool` for notification reliability.
 
 * Application logs handled by `monolog` and writing to `/var/log/exchangerate-app-error.log`, default php error redirecting to `/dev/stdout` can be tailing using following command.
 
    `docker logs -f mvf-exchange-rates-api`
  
#### Endpoints
`http://<docker-host>/test` - Test Endpoint

`http://<docker-host>/test/update_exchange_rates` - Update Exchange rates

`http://<docker-host>/test/convert_currency/<amount>/<from_currency>/<to_currency>` - Currency conversion endpoint 
 
 